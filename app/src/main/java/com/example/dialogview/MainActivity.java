package com.example.dialogview;

import android.Manifest;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button dialogView;
    private PublishDialog publishDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dialogView = (Button) findViewById(R.id.dialogView);
        dialogView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (publishDialog==null){
                    publishDialog=new PublishDialog(MainActivity.this);
                    publishDialog.setFabuClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(MainActivity.this, "发布", Toast.LENGTH_SHORT).show();
                        }
                    });
                    publishDialog.setHuishouClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(MainActivity.this, "回收", Toast.LENGTH_SHORT).show();

                        }
                    });
                    publishDialog.setPingguClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(MainActivity.this, "评估", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
                publishDialog.show();
            }
        });
    }
}
